
# Sunsat Link - BackEnd

O SunsatLink representa a iniciativa do Grupo 3 na disciplina de Projeto Integrador de Engenharia 2. Este grupo é composto por estudantes das áreas de engenharia aeroespacial, automotiva, eletrônica, energia e software, pertencentes ao Campus Faculdade do Gama da Universidade de Brasília. O propósito central deste projeto consiste em conceber uma solução de movimentação de uma antena de maneira que a mesma consiga captar corretamente os dados de um satélite pelo período em que o mesmo estiver visível a ela.

O servidor do projeto visa realizar todas as tratativas de usuários, além de realizar as tratativas iniciais dos valores de posicionamento da antena, informações que são consumidas por embarcados que as utilizam para movimentar a antena.

### Arquitetura do código

A arquitetura de software consiste na estrutura em que o sistema está organizado, quais os componentes o compõem, o grau de visibilidade destes para os elementos externos, e qual relação entre eles.

Como principal modelo arquitetural para estruturar o código do servidor do sistema e a interface foi adotada a arquitetura limpa. Criada por Robert C. Martin, a arquitetura limpa visa possibilitar que a lógica de negócio seja encapsulada e mantida separada do mecanismo de entrega. Esta separação ocorre por meio da divisão em camadas, podendo isolar as dependências externas da aplicação e facilitando a manutenção do código.

A camada de domínio é responsável pelas regras do negócio da aplicação, como os modelos de entidades, os erros, validações e interfaces dos repositórios. Na camada de aplicação está contida a lógica dos serviços oferecidos pelo sistema. A camada de apresentação é responsável pela comunicação do sistema com o ambiente externo, havendo a disponibilização dos serviços fornecidos pelo sistema, além de definir os contratos/protocolos das mensagens.

Na camada de infraestrutura e persistência ocorre a comunicação do sistema com demais dependências externas, como o consumo de serviço de terceiro e a utilização do sistema de banco de dados.

![Arquitetura Limpa](/assets/arquitetura_limpa.png)

Visando demonstrar como os pacotes estão estruturados na aplicação, a figura apresenta os diagrama de pacote do BackEnd.

![Diagrama de pacotes](/assets/backend-pacotes.png)

## Pontos a serem melhorados

A aplicação do BackEnd do SunSatLink buscou cumprir certos requisitos considerados essenciais para a aplicação como:

- Processamento de dados
- Gerenciamento de usuário
- Gerenciamento de satélite

Porém, alguns a aplicação possui alguns aspectos que podem ser melhoradas, como:

- Melhorar a tolerância à falha do sistema. Quando um apontamento inicial não é concluído e a aplicação é encerrada, ao tentar estabelecer uma nova conexão é apresentado erro quanto a ESP32 solicita as informações de apontamento.

- Adicionar um sistema de controle de token. Atualmente há um token de acesso, porém ele não está sendo utilizado nas requisições.

- Adicionar um sistema de cache para que as requisições recorrentes feitas para a API do N2YO demore menos.


## Execução do projeto

**Pré-requisito:** [Docker](https://www.docker.com/) e [Docker-compose](https://docs.docker.com/compose/)

1. Realizar o clone do repositório
2. Entrar na pasta do projeto
3. Executar o comando `docker-compose up`

- A aplicação estará rodando em `http://localhost:8085/`.
- A documentação dos endpoints fica no **/docs**, `http://localhost:8085/docs`